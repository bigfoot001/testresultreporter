package tools;

import org.apache.poi.common.usermodel.HyperlinkType;
import org.apache.poi.hssf.util.HSSFColor;
import org.apache.poi.ss.usermodel.*;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xddf.usermodel.chart.*;
import org.apache.poi.xssf.usermodel.*;
import pojo.environment;
import pojo.testrun;

import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class buildExcelWorkbook {
    private Workbook workBookQualityReport = new XSSFWorkbook();
    private dataBuilder dataBuilder = new dataBuilder();
    private CreationHelper creationHelper = workBookQualityReport.getCreationHelper();

    public Workbook createWorkbookQualityReport(environment testEnvironment) throws Exception {

        createQualityReportSheet(testEnvironment);

        ArrayList<ArrayList<testrun>> lastTwoRunsForAll = dataBuilder.getLastTwoResultsForComparison(testEnvironment);
        createCoverPageFailureAnalyses(testEnvironment,lastTwoRunsForAll);
        createSheetsFailureAnalyses(lastTwoRunsForAll.get(2), "NEW Failed");
        createSheetsFailureAnalyses(lastTwoRunsForAll.get(1),"Failed again");
        createSheetsFailureAnalyses(lastTwoRunsForAll.get(4),"Other");

        return workBookQualityReport;
    }

    public void createQualityReportSheet(environment testEnvironment) {
        XSSFSheet overallSummary = (XSSFSheet) workBookQualityReport.createSheet("Quality Index");

        createCells(overallSummary,60,30);

        setDataForGraphs(overallSummary,testEnvironment.getOther(),testEnvironment.getFailed(),testEnvironment.getPassed(),1,10,1,3);
        createGraphOtherFailedPassed(overallSummary,"Test Run Results by test run amount",1,20,11,38,2,11,1,2,3);

        setPercentageForGraph(overallSummary,20,10,3,2,10);
        createGraphOtherFailedPassed(overallSummary,"Test Run Results by %",12,1,22,19,21,30,1,2,3);

        setQualityIndexForGraph(overallSummary,40,10,1,21,10);
        createGraphQualityIndex(overallSummary,"Quality Index",1,1,11,19,41,50,1);

        createGraphTestRunAmount(overallSummary, "Change in Test Run amount by Days",12,20,22,38,2,10,1,2,3);

    }

    /**
     * Creates the cover page (summary) for the Failure analyses workbook
     * @param lastTwoRunsForAll list of all test runs grouped based on the comparison of the last 2 test run of each test case
     */
    private void createCoverPageFailureAnalyses (environment testEnvironment, ArrayList<ArrayList<testrun>> lastTwoRunsForAll) {
        String[] columnsTitles = new String[]{"execution","amount"};
//        String[] rowTitles = new String[]{"Passed again","Failed again","NEW Failed","NEW Passed","Other"};
        String[] rowTitles = new String[]{"Passed again","NEW Passed","Failed again","NEW Failed","Other"};
        XSSFSheet sheetLastResultsSummary = (XSSFSheet) workBookQualityReport.createSheet("Last Result Summary");
        createCells(sheetLastResultsSummary,30,30);

        createHeader(sheetLastResultsSummary,columnsTitles);

        Font summaryFont = workBookQualityReport.createFont();
        summaryFont.setColor(IndexedColors.BLACK.getIndex());
        CellStyle summaryCells = workBookQualityReport.createCellStyle();
        summaryCells.setAlignment(HorizontalAlignment.CENTER);
        summaryCells.setVerticalAlignment(VerticalAlignment.CENTER);
        summaryCells.setFont(summaryFont);


        //Row of Passed again
        sheetLastResultsSummary.getRow(1).getCell(0).setCellValue(rowTitles[0]);
        sheetLastResultsSummary.getRow(1).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(1).getCell(1).setCellValue(lastTwoRunsForAll.get(0).size());
        sheetLastResultsSummary.getRow(1).getCell(1).setCellStyle(summaryCells);

        //Row of NEW Passed
        sheetLastResultsSummary.getRow(2).getCell(0).setCellValue(rowTitles[1]);
        sheetLastResultsSummary.getRow(2).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(2).getCell(1).setCellValue(lastTwoRunsForAll.get(3).size());
        sheetLastResultsSummary.getRow(2).getCell(1).setCellStyle(summaryCells);

        //Row of Failed again
        sheetLastResultsSummary.getRow(3).getCell(0).setCellValue(rowTitles[2]);
        sheetLastResultsSummary.getRow(3).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(3).getCell(1).setCellValue(lastTwoRunsForAll.get(1).size());
        sheetLastResultsSummary.getRow(3).getCell(1).setCellStyle(summaryCells);

        //Row of NEW Failed
        sheetLastResultsSummary.getRow(4).getCell(0).setCellValue(rowTitles[3]);
        sheetLastResultsSummary.getRow(4).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(4).getCell(1).setCellValue(lastTwoRunsForAll.get(2).size());
        sheetLastResultsSummary.getRow(4).getCell(1).setCellStyle(summaryCells);

        //Row of Other
        sheetLastResultsSummary.getRow(5).getCell(0).setCellValue(rowTitles[4]);
        sheetLastResultsSummary.getRow(5).getCell(0).setCellStyle(summaryCells);
        sheetLastResultsSummary.getRow(5).getCell(1).setCellValue(lastTwoRunsForAll.get(4).size());
        sheetLastResultsSummary.getRow(5).getCell(1).setCellStyle(summaryCells);


        for ( int indexColumn = 0; indexColumn < 2; indexColumn++) {
            sheetLastResultsSummary.autoSizeColumn(indexColumn);
        }

        createClassificationGraph(sheetLastResultsSummary,testEnvironment,"Failures by Classification",3,0,13,18);

    }

    /**
     * This method create an additional sheet which shows test runs with details like: market, feature, title, url.
     * @param testrunList is list of test runs which previously passed, but last time failed.
     */
    private void createSheetsFailureAnalyses(ArrayList<testrun> testrunList, String sheetTitle ){
        //Create sheet
        XSSFSheet sheet = (XSSFSheet) workBookQualityReport.createSheet(sheetTitle);
        createCells(sheet,1,10);
        // SET the Styles
        //GENERIC CELL
        CellStyle genericCell = workBookQualityReport.createCellStyle();
        genericCell.setAlignment(HorizontalAlignment.LEFT);
        genericCell.setVerticalAlignment(VerticalAlignment.CENTER);
        //HIGHLIGHTED CELL
        Font highlightedFont = workBookQualityReport.createFont();
        highlightedFont.setBold(true);
        highlightedFont.setColor(HSSFColor.HSSFColorPredefined.RED.getIndex());
        CellStyle highlightedCell = workBookQualityReport.createCellStyle();
        highlightedCell.setFont(highlightedFont);
        highlightedCell.setAlignment(HorizontalAlignment.LEFT);
        highlightedCell.setVerticalAlignment(VerticalAlignment.CENTER);
        highlightedCell.setFillBackgroundColor(HSSFColor.HSSFColorPredefined.AQUA.getIndex());
        //URL CELL
        Font urlFont = workBookQualityReport.createFont();
        urlFont.setColor(IndexedColors.BLUE.getIndex());
        CellStyle urlCell = workBookQualityReport.createCellStyle();
        urlCell.setFont(urlFont);
        urlCell.setAlignment(HorizontalAlignment.CENTER);
        urlCell.setVerticalAlignment(VerticalAlignment.CENTER);

        String[] columnsTitles;
        Integer rowPosition = 1;
        if (testrunList.get(0).getExecutions().get(0).getResult().equalsIgnoreCase("failed") &&
                testrunList.get(0).getExecutions().get(1).getResult().equalsIgnoreCase("failed")){
            columnsTitles = new String[]{"Market","Feature","Test Run ID","Test Case title", "Current Exception", "Previous Exception", "Failed Step", "Classification"};
            createHeader(sheet,columnsTitles);
            // Create the Cells and fill them with data, with added style
            for ( int indexTestRun = 0; indexTestRun < testrunList.size(); indexTestRun++){
                Row temporaryRow = sheet.createRow(rowPosition);
                temporaryRow.createCell(0).setCellValue(testrunList.get(indexTestRun).getMarket());
                temporaryRow.getCell(0).setCellStyle(genericCell);
                temporaryRow.createCell(1).setCellValue(testrunList.get(indexTestRun).getFeature());
                temporaryRow.getCell(1).setCellStyle(genericCell);
                XSSFHyperlink hyperlink = (XSSFHyperlink) creationHelper.createHyperlink(HyperlinkType.URL);
                hyperlink.setAddress(testrunList.get(indexTestRun).getUrlWebpage());
                temporaryRow.createCell(2).setCellValue(testrunList.get(indexTestRun).getTestRunName());
                temporaryRow.getCell(2).setHyperlink(hyperlink);
                temporaryRow.getCell(2).setCellStyle(urlCell);
                temporaryRow.createCell(3).setCellValue(testrunList.get(indexTestRun).getTestCaseName());
                temporaryRow.getCell(3).setCellStyle(genericCell);
                temporaryRow.createCell(4).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getCauseOfFailure());
                if (testrunList.get(0).getExecutions().get(0).getCauseOfFailure().equalsIgnoreCase(testrunList.get(0).getExecutions().get(1).getCauseOfFailure())){
                    temporaryRow.createCell(5).setCellValue("SAME EXCEPTION");
                    temporaryRow.getCell(4).setCellStyle(genericCell);
                    temporaryRow.getCell(5).setCellStyle(genericCell);
                } else {
                    temporaryRow.createCell(5).setCellValue(testrunList.get(indexTestRun).getExecutions().get(1).getCauseOfFailure());
                    temporaryRow.getCell(4).setCellStyle(highlightedCell);
                    temporaryRow.getCell(5).setCellStyle(genericCell);
                }
                temporaryRow.createCell(6).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getStepOfFailure());
                if (testrunList.get(indexTestRun).getExecutions().get(0).getClassificationOfFailure().isEmpty()){
                    temporaryRow.createCell(7).setCellValue("MANUAL CHECK NECESSARY");
                    temporaryRow.getCell(7).setCellStyle(highlightedCell);
                    sheet.autoSizeColumn(7);
                } else {
                    temporaryRow.createCell(7).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getClassificationOfFailure());
                    temporaryRow.getCell(7).setCellStyle(genericCell);
                    sheet.autoSizeColumn(7);
                }
                rowPosition++;
            }
        } else if (testrunList.get(0).getExecutions().get(0).getResult().equalsIgnoreCase("failed") &&
            !testrunList.get(0).getExecutions().get(0).getResult().equalsIgnoreCase(testrunList.get(0).getExecutions().get(1).getResult())) {
            columnsTitles = new String[]{"Market","Feature","Test Run ID","Test Case title", "Current Exception", "Failed Step", "Classification"};
            createHeader(sheet,columnsTitles);
            // Create the Cells and fill them with data, with added style
            for ( int indexTestRun = 0; indexTestRun < testrunList.size(); indexTestRun++) {
                Row temporaryRow = sheet.createRow(rowPosition);
                temporaryRow.createCell(0).setCellValue(testrunList.get(indexTestRun).getMarket());
                temporaryRow.getCell(0).setCellStyle(genericCell);
                temporaryRow.createCell(1).setCellValue(testrunList.get(indexTestRun).getFeature());
                temporaryRow.getCell(1).setCellStyle(genericCell);
                XSSFHyperlink hyperlink = (XSSFHyperlink) creationHelper.createHyperlink(HyperlinkType.URL);
                hyperlink.setAddress(testrunList.get(indexTestRun).getUrlWebpage());
                temporaryRow.createCell(2).setCellValue(testrunList.get(indexTestRun).getTestRunName());
                temporaryRow.getCell(2).setHyperlink(hyperlink);
                temporaryRow.getCell(2).setCellStyle(urlCell);
                temporaryRow.createCell(3).setCellValue(testrunList.get(indexTestRun).getTestCaseName());
                temporaryRow.getCell(3).setCellStyle(genericCell);
                temporaryRow.createCell(4).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getCauseOfFailure());
                temporaryRow.getCell(4).setCellStyle(genericCell);
                temporaryRow.createCell(5).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getStepOfFailure());
                if (testrunList.get(indexTestRun).getExecutions().get(0).getClassificationOfFailure().isEmpty()){
                    temporaryRow.createCell(6).setCellValue("MANUAL CHECK NECESSARY");
                    temporaryRow.getCell(6).setCellStyle(highlightedCell);
                    sheet.autoSizeColumn(6);
                } else {
                    temporaryRow.createCell(6).setCellValue(testrunList.get(indexTestRun).getExecutions().get(0).getClassificationOfFailure());
                    temporaryRow.getCell(6).setCellStyle(genericCell);
                    sheet.autoSizeColumn(6);
                }

                rowPosition++;
            }
        }
        else {
            columnsTitles = new String[]{"Market","Feature","Test Run ID","Test Case title"};
            createHeader(sheet,columnsTitles);
            // Create the Cells and fill them with data, with added style
            for ( int indexTestRun = 0; indexTestRun < testrunList.size(); indexTestRun++){
                Row temporaryRow = sheet.createRow(rowPosition);
                temporaryRow.createCell(0).setCellValue(testrunList.get(indexTestRun).getMarket());
                temporaryRow.getCell(0).setCellStyle(genericCell);
                temporaryRow.createCell(1).setCellValue(testrunList.get(indexTestRun).getFeature());
                temporaryRow.getCell(1).setCellStyle(genericCell);
                temporaryRow.createCell(2).setCellFormula(
                        "HYPERLINK" +
                                "(\""+testrunList.get(indexTestRun).getUrlWebpage()+"\"," +
                                "\""+testrunList.get(indexTestRun).getTestRunName()+"\")");
                temporaryRow.getCell(2).setCellStyle(urlCell);
                temporaryRow.createCell(3).setCellValue(testrunList.get(indexTestRun).getTestCaseName());
                temporaryRow.getCell(3).setCellStyle(genericCell);
                rowPosition++;
            }
        }

        // AUTOSIZE all columns
        for ( int indexColumn = 0; indexColumn < 4; indexColumn++) {
            sheet.autoSizeColumn(indexColumn);
        }
        for ( int indexColumn = 6; indexColumn < 8; indexColumn++) {
            sheet.autoSizeColumn(indexColumn);
        }
    }

    /**
     * This method creates the Header (1st row) in the given sheet.
     * @param sheet sheet which will contain the header
     * @param columns collumn titles
     */
    private void createHeader (Sheet sheet, String[] columns){
        Font headerFont = workBookQualityReport.createFont();
        headerFont.setBold(true);
        headerFont.setFontHeightInPoints((short) 12);
        headerFont.setColor(IndexedColors.BLACK.getIndex());
        CellStyle headerCellStyle = workBookQualityReport.createCellStyle();
        headerCellStyle.setFont(headerFont);
        headerCellStyle.setLocked(true);
        headerCellStyle.setAlignment(HorizontalAlignment.CENTER);
        headerCellStyle.setVerticalAlignment(VerticalAlignment.CENTER);
        Row headerRow = sheet.getRow(0);
        for(int i = 0; i < columns.length; i++) {
            Cell cell = headerRow.getCell(i);
            cell.setCellValue(columns[i]);
            cell.setCellStyle(headerCellStyle);
        }

        for ( int indexColumn = 0; indexColumn < columns.length; indexColumn++) {
            sheet.autoSizeColumn(indexColumn);
        }
    }

    private void createCells (XSSFSheet sheet, Integer rowAmount, Integer columnAmount){
        rowAmount=rowAmount+1; //userfriendly row number converted to id
        columnAmount=columnAmount+1; //userfriendly row number converted to id
        for (int rowIndex = 0; rowIndex < rowAmount; rowIndex++) {
            sheet.createRow(rowIndex);
            for (int columnIndex = 0; columnIndex < columnAmount; columnIndex++) {
                sheet.getRow(rowIndex).createCell(columnIndex);
            }
        }
    }

    /**
     * This method creates an xlsx file from workbook.
     * @throws IOException ...
     */
    public void createFile (Workbook workbook) throws IOException {
        // PREPARE the random value for name
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH-mm");
        String random = dateFormat.format(new Date());
        //CREATE file
        FileOutputStream fileOut = new FileOutputStream("autogeneratedReport_"+random+".xlsx");
        workbook.write(fileOut);
        fileOut.close();
        workbook.close();
    }

    private void setDataForGraphs(XSSFSheet sheet, Integer[] other, Integer[] failed, Integer[] passed, Integer rowBegin, Integer amountOfRows, Integer columnBegin, Integer amountOfColumns) {
        rowBegin=rowBegin-1; //userfriendly row number converted to id

        sheet.getRow(rowBegin).getCell(0).setCellValue("OTHER");
        sheet.getRow(rowBegin).getCell(1).setCellValue("FAILED");
        sheet.getRow(rowBegin).getCell(2).setCellValue("PASSED");

        Integer rowIndex = 1;
        for (int resultIndex = (other.length-1); resultIndex >= 0; resultIndex--){
            sheet.getRow(rowIndex).getCell(0).setCellValue(other[resultIndex]);
            rowIndex++;
        }
        rowIndex=1;
        for (int resultIndex = (failed.length-1); resultIndex >= 0; resultIndex--) {
            sheet.getRow(rowIndex).getCell(1).setCellValue(failed[resultIndex]);
            rowIndex++;
        }
        rowIndex=1;
        for (int resultIndex = (passed.length-1); resultIndex >= 0; resultIndex--) {
            sheet.getRow(rowIndex).getCell(2).setCellValue(passed[resultIndex]);
            rowIndex++;
        }
    }

    private void setPercentageForGraph(XSSFSheet sheet, Integer rowBegin, Integer amountOfRows, Integer amountOfColumns, Integer dataRowBegin, Integer dataAmountOfRows) {
        rowBegin=rowBegin-1; //userfriendly row number converted to id
        dataRowBegin=dataRowBegin-1; //userfriendly row number converted to id

        sheet.getRow(rowBegin).getCell(0).setCellValue("OTHER");
        sheet.getRow(rowBegin).getCell(1).setCellValue("FAILED");
        sheet.getRow(rowBegin).getCell(2).setCellValue("PASSED");

        CellStyle percentage = sheet.getWorkbook().createCellStyle();
        percentage.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(9)));

        Integer rowIndexPercentage =rowBegin+1;
        for (int rowIndex = dataRowBegin; rowIndex < (dataRowBegin+dataAmountOfRows); rowIndex++){
            for (int columnIndex = 0; columnIndex < 3; columnIndex++) {
                sheet.getRow(rowIndexPercentage).getCell(columnIndex).setCellStyle(percentage);
                sheet.getRow(rowIndexPercentage).getCell(columnIndex).setCellValue(
                        (Float.parseFloat(String.valueOf(sheet.getRow(rowIndex).getCell(columnIndex).getNumericCellValue())) /
                                (Float.parseFloat(String.valueOf(sheet.getRow(rowIndex).getCell(0).getNumericCellValue())) +
                                        Float.parseFloat(String.valueOf(sheet.getRow(rowIndex).getCell(1).getNumericCellValue())) +
                                        Float.parseFloat(String.valueOf(sheet.getRow(rowIndex).getCell(2).getNumericCellValue()))
                                )
                        )
                );
            }
            rowIndexPercentage++;
        }
    }

    private void setQualityIndexForGraph(XSSFSheet sheet, Integer rowBegin, Integer amountOfRows, Integer amountOfColumns, Integer dataRowBegin, Integer dataAmountOfRows) {
        rowBegin=rowBegin-1; //userfriendly row number converted to id
        dataRowBegin=dataRowBegin-1; //userfriendly row number converted to id

        sheet.getRow(rowBegin).getCell(0).setCellValue("Quality Index");

        CellStyle percentage = sheet.getWorkbook().createCellStyle();
        percentage.setDataFormat(sheet.getWorkbook().createDataFormat().getFormat(BuiltinFormats.getBuiltinFormat(9)));

        Integer rowQualityIndex =rowBegin+1;
        for (int rowIndex = dataRowBegin; rowIndex < (dataRowBegin+dataAmountOfRows); rowIndex++){
                sheet.getRow(rowQualityIndex).getCell(0).setCellStyle(percentage);
                sheet.getRow(rowQualityIndex).getCell(0).setCellValue(
                        (Float.parseFloat(String.valueOf(sheet.getRow(rowIndex).getCell(2).getNumericCellValue())) -
                                Float.parseFloat(String.valueOf(sheet.getRow(rowIndex).getCell(1).getNumericCellValue()))
                        )
                );
            rowQualityIndex++;
        }
    }

    private void createGraphOtherFailedPassed(XSSFSheet sheet, String title, Integer graphX0, Integer graphY0, Integer graphX1, Integer graphY1, Integer firstRow, Integer lastRow, Integer colOther, Integer colFailed, Integer colPassed) {
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = drawing.createAnchor(0,0,0,0,graphX0-1,graphY0-1,graphX1-1,graphY1-1);
        XSSFChart chart = drawing.createChart(anchor);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(org.apache.poi.xddf.usermodel.chart.LegendPosition.BOTTOM);
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.LEFT);
        leftAxis.setCrosses(org.apache.poi.xddf.usermodel.chart.AxisCrosses.AUTO_ZERO);

        XDDFDataSource<String> xs = XDDFDataSourcesFactory.fromArray(new String[]{"10th","9th","8th","7th","6th","5th","4th","3rd","2nd","last run"});
        XDDFNumericalDataSource ys1 = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRow-1, lastRow-1, colOther-1, colOther-1));
        XDDFNumericalDataSource ys2 = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRow-1, lastRow-1, colFailed-1, colFailed-1));
        XDDFNumericalDataSource ys3 =  XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRow-1, lastRow-1, colPassed-1, colPassed-1));

        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE,bottomAxis,leftAxis);
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series) data.addSeries(xs,ys1);
        series1.setTitle("Other",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series2 = (XDDFLineChartData.Series) data.addSeries(xs,ys2);
        series2.setTitle("Failed",null);
        series2.setSmooth(true);
        series2.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series3 = (XDDFLineChartData.Series) data.addSeries(xs,ys3);
        series3.setTitle("Passed",null);
        series3.setSmooth(true);
        series3.setMarkerStyle(MarkerStyle.SQUARE);

        chart.setTitleText(title);
        chart.plot(data);
    }

    private void createGraphQualityIndex(XSSFSheet sheet, String title, Integer graphX0, Integer graphY0, Integer graphX1, Integer graphY1, Integer firstRow, Integer lastRow, Integer colQualityIndex) {
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = drawing.createAnchor(0,0,0,0,graphX0-1,graphY0-1,graphX1-1,graphY1-1);
        XSSFChart chart = drawing.createChart(anchor);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(org.apache.poi.xddf.usermodel.chart.LegendPosition.BOTTOM);
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.LEFT);
        leftAxis.setCrosses(org.apache.poi.xddf.usermodel.chart.AxisCrosses.AUTO_ZERO);

        XDDFDataSource<String> xs = XDDFDataSourcesFactory.fromArray(new String[]{"10th","9th","8th","7th","6th","5th","4th","3rd","2nd","last run"});
        XDDFNumericalDataSource ys1 = XDDFDataSourcesFactory.fromNumericCellRange(sheet, new CellRangeAddress(firstRow-1, lastRow-1, colQualityIndex-1, colQualityIndex-1));
        XDDFNumericalDataSource ys2 = XDDFDataSourcesFactory.fromArray(new Number[]{0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,0.8,});

        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE,bottomAxis,leftAxis);
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series) data.addSeries(xs,ys1);
        series1.setTitle("Quality Index",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series2 = (XDDFLineChartData.Series) data.addSeries(xs,ys2);
        series2.setTitle("Quality Minimum",null);
        series2.setSmooth(false);
        series2.setMarkerStyle(MarkerStyle.NONE);

        chart.setTitleText(title);
        chart.plot(data);
    }

    private void createGraphTestRunAmount(XSSFSheet sheet, String title, Integer graphX0, Integer graphY0, Integer graphX1, Integer graphY1, Integer dataRowBegin, Integer dataAmountOfRows, Integer colOther, Integer colFailed, Integer colPassed){
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = drawing.createAnchor(0,0,0,0,graphX0-1,graphY0-1,graphX1-1,graphY1-1);
        XSSFChart chart = drawing.createChart(anchor);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(org.apache.poi.xddf.usermodel.chart.LegendPosition.BOTTOM);
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.LEFT);
        leftAxis.setCrosses(org.apache.poi.xddf.usermodel.chart.AxisCrosses.AUTO_ZERO);

        XDDFDataSource<String> xs = XDDFDataSourcesFactory.fromArray(new String[]{"10th","9th","8th","7th","6th","5th","4th","3rd","2nd","last run"});
        Number[] testAmountArray = new Number[dataAmountOfRows];
        for (int index = 0; index < testAmountArray.length; index++){
            testAmountArray[index]=(sheet.getRow(dataRowBegin-1).getCell(colFailed-1).getNumericCellValue()+
                    sheet.getRow(dataRowBegin-1).getCell(colOther-1).getNumericCellValue()+
                    sheet.getRow(dataRowBegin-1).getCell(colPassed-1).getNumericCellValue());
            dataRowBegin++;
        }

        XDDFNumericalDataSource ys1 = XDDFDataSourcesFactory.fromArray(testAmountArray);

        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE,bottomAxis,leftAxis);
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series) data.addSeries(xs,ys1);
        series1.setTitle("Test Run amount",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        chart.setTitleText(title);

        chart.plot(data);
    }

    private void createClassificationGraph(XSSFSheet sheet, environment testEnvironment, String title, Integer graphX0, Integer graphY0, Integer graphX1, Integer graphY1){
        XSSFDrawing drawing = sheet.createDrawingPatriarch();
        XSSFClientAnchor anchor = drawing.createAnchor(0,0,0,0,graphX0-1,graphY0-1,graphX1-1,graphY1-1);
        XSSFChart chart = drawing.createChart(anchor);
        XDDFChartLegend legend = chart.getOrAddLegend();
        legend.setPosition(org.apache.poi.xddf.usermodel.chart.LegendPosition.BOTTOM);
        XDDFCategoryAxis bottomAxis = chart.createCategoryAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.BOTTOM);
        XDDFValueAxis leftAxis = chart.createValueAxis(org.apache.poi.xddf.usermodel.chart.AxisPosition.LEFT);
        leftAxis.setCrosses(org.apache.poi.xddf.usermodel.chart.AxisCrosses.AUTO_ZERO);

        XDDFDataSource<String> xs = XDDFDataSourcesFactory.fromArray(new String[]{"10th","9th","8th","7th","6th","5th","4th","3rd","2nd","last run"});
        XDDFNumericalDataSource ySeriesApplicationDefect = XDDFDataSourcesFactory.fromArray(dataBuilder.getClassificationAmount(testEnvironment, "Application Defect",10));
        XDDFNumericalDataSource ySeriesScriptIssue = XDDFDataSourcesFactory.fromArray(dataBuilder.getClassificationAmount(testEnvironment, "Script issue",10));
        XDDFNumericalDataSource ySeriesContentIssue = XDDFDataSourcesFactory.fromArray(dataBuilder.getClassificationAmount(testEnvironment, "Content issue",10));
        XDDFNumericalDataSource ySeriesAccessIssue = XDDFDataSourcesFactory.fromArray(dataBuilder.getClassificationAmount(testEnvironment, "Access issue",10));
        XDDFNumericalDataSource ySeriesDataIssue = XDDFDataSourcesFactory.fromArray(dataBuilder.getClassificationAmount(testEnvironment, "Data issue",10));
        XDDFNumericalDataSource ySeriesConfigurationIssue = XDDFDataSourcesFactory.fromArray(dataBuilder.getClassificationAmount(testEnvironment, "Configuration issue",10));
        XDDFNumericalDataSource ySeriesEnvironmentIssue = XDDFDataSourcesFactory.fromArray(dataBuilder.getClassificationAmount(testEnvironment, "Environment issue",10));
        XDDFNumericalDataSource ySeriesNotClassifiedIssue = XDDFDataSourcesFactory.fromArray(dataBuilder.getClassificationAmount(testEnvironment, "Not Classified",10));

        XDDFLineChartData data = (XDDFLineChartData) chart.createData(ChartTypes.LINE,bottomAxis,leftAxis);
        XDDFLineChartData.Series series1 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesApplicationDefect);
        series1.setTitle("Application Defect",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series2 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesScriptIssue);
        series2.setTitle("Script Issue",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series3 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesContentIssue);
        series3.setTitle("Content Issue",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series4 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesAccessIssue);
        series4.setTitle("Access Issue",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series5 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesDataIssue);
        series5.setTitle("Data Issue",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series6 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesConfigurationIssue);
        series6.setTitle("Configuration Issue",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series7 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesEnvironmentIssue);
        series7.setTitle("Environment Issue",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        XDDFLineChartData.Series series8 = (XDDFLineChartData.Series) data.addSeries(xs,ySeriesNotClassifiedIssue);
        series8.setTitle("Not Classified",null);
        series1.setSmooth(true);
        series1.setMarkerStyle(MarkerStyle.SQUARE);

        chart.setTitleText(title);
        chart.plot(data);
    }
}
