package tools;

import enums.failureClassification;
import enums.failureManualVerification;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.util.EntityUtils;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.qas.qtest.api.services.execution.model.TestCycle;
import pojo.*;

import java.io.IOException;
import java.io.InputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.Properties;

public class dataBuilder {

    /**
     *  This method is getting the full list of the test runs in an HttpEntity, and transforming it into a JSONArray.
     * @param inputEntity HttpEntity which contains JSON object(s).
     * @return Output will be a JSONArray which contains the list of JSONObjects under the "items" branch.
     * @throws IOException If something fails during the httpEntity->String transformation.
     * @throws org.json.simple.parser.ParseException If the JSON parser fails to parse the String.
     */
    public JSONArray getTestRunsFromResponse(HttpEntity inputEntity) throws IOException, org.json.simple.parser.ParseException {
        String responseStringRunResults = EntityUtils.toString(inputEntity);
        JSONObject fullResponseRunResults = (JSONObject) new JSONParser().parse(responseStringRunResults);
        return (JSONArray) fullResponseRunResults.get("items");
    }

    /**
     *  This method adding the basic data into the test run, like: ids, versions, name, etc.
     * @param selectedRun is  JSONObject which has the details for one test run
     * @param currentTestRun is a "test run" object for later usage
     */
    private void addBasicDataToSelectedRun(JSONObject selectedRun, testrun currentTestRun){
        currentTestRun.setTestCaseName((String)selectedRun.get("name"));
        currentTestRun.setTestRunName((String)selectedRun.get("pid"));
        currentTestRun.setTestCaseId((Long)selectedRun.get("testCaseId"));
        currentTestRun.setTestCaseVersion((String)selectedRun.get("test_case_version"));
        currentTestRun.setTestCaseVersionId((Long)selectedRun.get("test_case_version_id"));
        currentTestRun.setTestRunParentId((Long)selectedRun.get("parentId"));
        currentTestRun.setTestRunParentType((String)selectedRun.get("parentType"));
    }

    /**
     * This method is adding Status, Env and Market into the test run.
     * The path to get these data is different than in case of the "BasicData".
     * @param currentTestRun is the "test run" object which we would like to update with Status (last run), Env, Market.
     * @param propertiesCurrentRun is the details of the given run
     */
    private void addBasicPropertiesToSelectedRun (testrun currentTestRun, JSONArray propertiesCurrentRun){
        for ( int subindexRuns = 0; subindexRuns < propertiesCurrentRun.size(); subindexRuns++ ) {
            JSONObject propertyOfRun = (JSONObject) propertiesCurrentRun.get(subindexRuns);
            if ( propertyOfRun.get("field_name").toString().equalsIgnoreCase("Status") ) {
                currentTestRun.setLastStatus(propertyOfRun.get("field_value_name").toString());
            }
            if ( propertyOfRun.get("field_name").toString().equalsIgnoreCase("Env") ) {
                currentTestRun.setEnv(propertyOfRun.get("field_value_name").toString());
            }
            if ( propertyOfRun.get("field_name").toString().equalsIgnoreCase("Market") ) {
                currentTestRun.setMarket(propertyOfRun.get("field_value_name").toString());
            }
        }
    }

    /**
     *  This method is adding the last results into the test run (based on the size of the array) and sorting them descending.
     * @param currentTestRun is the "test run" object which we would like to update with last X run results
     * @param arrayOfRunResults is the JSONArray which contains the list of JSONObjects (the earlier test runs)
     */
    private void addLastRunResultsToSelectedRun(testrun currentTestRun, JSONArray arrayOfRunResults) throws ParseException {
        ArrayList<execution> executionsTemporary = new ArrayList<>();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        for ( int subindexResults = 0 ; subindexResults < arrayOfRunResults.size(); subindexResults++) {
            JSONObject resultSelectedRun = (JSONObject) arrayOfRunResults.get(subindexResults);
            JSONObject statusSelectedRun = (JSONObject) resultSelectedRun.get("status");
            String statusOfSelectedRun = statusSelectedRun.get("name").toString();
            String executionEndDateSelectedRun = (String) resultSelectedRun.get("exe_end_date");
            Date executionEndDate = dateFormat.parse(executionEndDateSelectedRun);
            String executionStartDateSelectedRun = (String) resultSelectedRun.get("exe_start_date");
            Date executionStartDate = dateFormat.parse(executionEndDateSelectedRun);
            execution currentExecution = new execution(statusOfSelectedRun, executionEndDate, executionStartDate);
            //create the execution if it has failed
            if (statusOfSelectedRun.equalsIgnoreCase("failed")){
                JSONArray propertiesOfRun = (JSONArray) resultSelectedRun.get("properties");
                JSONArray testStepsLog = (JSONArray) resultSelectedRun.get("test_step_logs");

                //SET STACKTRACE FOR FAUILURE
                String traceFull = "";
                for (int i = 0; i < propertiesOfRun.size(); i++){
                    JSONObject property = (JSONObject) propertiesOfRun.get(i);
                    if (property.get("field_name").toString().equalsIgnoreCase("Automated test stacktrace")){
                        traceFull = (String) property.get("field_value");
                        break;
                    }
                }
                //Check if trace is longer than 256 character, and set the cutting point at at first 'new line' or at 255 index.
                Integer indexOfCut = 0;
                if (traceFull.indexOf("\n") >=255 ) {
                    indexOfCut = 255;
                } else {
                    indexOfCut = traceFull.indexOf("\n");
                }
                //Check if indexOfCut is -1 (which means the trace length is less than 256 and there is no 'new line')
                //set trace into the execution
                if (indexOfCut == -1){
                    if (traceFull.length()>=255) {
                        currentExecution.setCauseOfFailure(traceFull.substring(0, 255));
                    } else {currentExecution.setCauseOfFailure(traceFull);}
                } else {
                    currentExecution.setCauseOfFailure(traceFull.substring(0,(indexOfCut)));
                }

                //SET CLASSIFICATION
                String classification = "";
                for (int i = 0; i < propertiesOfRun.size(); i++){
                    JSONObject property = (JSONObject) propertiesOfRun.get(i);
                    if (property.get("field_name").toString().equalsIgnoreCase("Automation fail clasyfication")){
                        currentExecution.setClassificationOfFailure((String) property.get("field_value_name"));
                        break;
                    }
                }
                //SET VERIFICATION
                for (int i = 0; i < propertiesOfRun.size(); i++){
                    JSONObject property = (JSONObject) propertiesOfRun.get(i);
                    if (property.get("field_name").toString().equalsIgnoreCase("Automation fail manual veryfication")){
                        currentExecution.setVerificationOfFailure((String) property.get("field_value_name"));
                        break;
                    }
                }
                //SET COMMENT
                for (int i = 0; i < propertiesOfRun.size(); i++){
                    JSONObject property = (JSONObject) propertiesOfRun.get(i);
                    if (property.get("field_name").toString().equalsIgnoreCase("Automation fail comment")){
                        currentExecution.setVerificationOfFailure((String) property.get("field_value"));
                        break;
                    }
                }

                //SET STEP
                for (int i = 0; i < testStepsLog.size(); i++){
                    JSONObject step = (JSONObject) testStepsLog.get(i);
                    if (((JSONObject)step.get("status")).get("name").toString().equalsIgnoreCase("failed")){
                        currentExecution.setStepOfFailure(String.valueOf(step.get("order")));
                        break;
                    }
                }

                executionsTemporary.add(currentExecution);
            // create the execution if it has NOT failed
            } else {
                executionsTemporary.add(currentExecution);
            }
        }
        Collections.sort(executionsTemporary, (a, b) -> b.getExecutionEndDate().compareTo(a.getExecutionEndDate()));

        currentTestRun.setExecutions(executionsTemporary);
    }

    /**
     *  This method is creating a testrun[] Array, listing all the input test runs with necessary attributes.
     *  Outcome will be used to map the data in the Env>>Market>>Feature structure.
     * @param arrayOfTestRuns is the full list of TestRuns (JSONObjects) in a JSONArray
     * @param projectId is related to the project in qTest which you are interested in
     * @param bearer can be copied from qTest after you logged into, and checked "API&SDK" section in the "Download qTest Resources" page.
     * @param pageSizeRunResults is setting how many results you would like to see on 1 page (max is 999)
     * @return an array of test runs for later usages
     * @throws Exception ...
     */
    public testrun[] createTestRuns(JSONArray arrayOfTestRuns, String projectId, String bearer, Integer pageSizeRunResults) throws Exception {
        httpRequestsGet requestHandler = new httpRequestsGet();
        testrun[] testruns = new testrun[arrayOfTestRuns.size()];
        Integer indexTestRun = 0 ;
        for ( int index = 0; index < arrayOfTestRuns.size(); index++ ) {
            /*  CREATE THE TEST-RUN  */
            JSONObject selectedRun = (JSONObject) arrayOfTestRuns.get(index);
            testrun currentTestRun = new testrun((Long) selectedRun.get("id"));
            /*  ADD DATA INTO THE TEST-RUN  */
            addBasicDataToSelectedRun(selectedRun,currentTestRun);
            addBasicPropertiesToSelectedRun(currentTestRun,(JSONArray) selectedRun.get("properties"));
            /*  ADD THE LAST RUNS INTO THE TEST-RUN  */
            addLastRunResultsToSelectedRun(
                    currentTestRun,
                    getTestRunsFromResponse(requestHandler.getTestRunLastResults(projectId,currentTestRun.getTestRunId().toString(),pageSizeRunResults.toString(),bearer))
            );
            JSONArray linksSelectedRun = (JSONArray) selectedRun.get("links");
            JSONObject selfUrlApi = (JSONObject) linksSelectedRun.get(2);
            currentTestRun.setUrlApi(selfUrlApi.get("href").toString());
            currentTestRun.setUrlWebpage("https://avon.qtestnet.com/p/"+projectId+"/portal/project#tab=testexecution&object=3&id="+currentTestRun.getTestRunId().toString());
            /*  ADD THE CURRENT TEST-RUN INTO THE LIST  */
            testruns[index]= currentTestRun;
        }
        return testruns;
    }

    /**
     *  This method is creating the environment object with the structure of Environment > Market > Features
     * @param projectId is related to the project in qTest which you are interested in
     * @param testCycleId is one of the test cycles of PROD, QAM or QAF Automation root test cycles
     * @return the created environment with Markets (Features included)
     * @throws IOException ...
     */
    public environment createTestEnvironment (Long projectId, Long testCycleId) throws IOException {
        httpRequestsGet requestCreator = new httpRequestsGet();
        /*  GETTING THE TEST CYCLES */
        TestCycle testCycles = requestCreator.getAllTestCyclesInTestCycle(projectId,testCycleId);
        /*  CREATE THE ENVIRONMENT  */
        environment testEnvironment = new environment(testCycles.getName(), testCycles.getId(), projectId);
        /*  ADD THE MARKETS WITH FEATURES INTO THE ENVIRONMENT  */
        testEnvironment.setMarkets(
                provideMarketsIntoEnvironment(testCycles)
        );

        return testEnvironment;
    }

    /**
     * This method is putting all the "test run"s under the correct place in the Environment > Market > Feature structure.
     * @param testruns is the Array of the "test run"s got from qTest
     * @param testEnvironment is a previously create environment object
     */
    public void buildUpTestEnvironment(testrun[] testruns, environment testEnvironment, Long projectId){
        for (int indexRunAmount = 0; indexRunAmount < testruns.length; indexRunAmount++) {
            for (int indexMarketAmount = 0; indexMarketAmount < testEnvironment.getMarkets().length; indexMarketAmount++) {
                for (int indexFeatureAmount = 0; indexFeatureAmount < testEnvironment.getMarkets()[indexMarketAmount].getFeatures().length; indexFeatureAmount ++){
                    if (testruns[indexRunAmount].getTestRunParentId().longValue() == testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestCycleId().longValue()) {
                        testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestruns().add(testruns[indexRunAmount]);
                        String featureName = "";
                        testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestruns()
                                .get(testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestruns().size()-1)
                                .setFeature(testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getName().
                                        substring(0,testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getName().length()-8));
                    }
                }
            }
        }
        setTestRunAmounts(testEnvironment);
        setResultAmountFeature(testEnvironment);
        setResultAmountMarket(testEnvironment);
        setResultAmountEnv(testEnvironment);
        System.out.println("BREAK POINT");
    }

    /**
     *  This method is creating the market object with the structure of Market > Features
     * @param testCycles is one of the test cycles of PROD, QAM or QAF Automation root test cycles
     * @return the Markets (Features included)
     */
    private market[] provideMarketsIntoEnvironment(TestCycle testCycles){
        /*  CREATE THE LIST OF MARKETS WITH FEATURES  */
        market[] testMarkets = new market[testCycles.getTestCycles().size()];
        for (int index = 0; index < testCycles.getTestCycles().size(); index++) {
            testMarkets[index] = new market(testCycles.getTestCycles().get(index).getName(), testCycles.getTestCycles().get(index).getId());
            feature[] testFeatures = new feature[testCycles.getTestCycles().get(index).getTestCycles().get(0).getTestCycles().size()];
            for (int subindex = 0; subindex < testFeatures.length; subindex++) {
                testFeatures[subindex] = new feature(
                        testCycles.getTestCycles().get(index).getTestCycles().get(0).getTestCycles().get(subindex).getName(),
                        testCycles.getTestCycles().get(index).getTestCycles().get(0).getTestCycles().get(subindex).getId()
                );
            }
            testMarkets[index].setFeatures(testFeatures);
        }
        return testMarkets;
    }

    /**
     *  This method organizes the test runs into "Passed Again", "Failed Again", "NEW Failure", "NEW Pass" groups.
     * @param testEnvironment  is the test environment which you are working with.
     * @return the ArrayList of the the mentioned test runs' groups.
     */
    public ArrayList<ArrayList<testrun>> getLastTwoResultsForComparison (environment testEnvironment) {
        ArrayList<ArrayList<testrun>> summary = new ArrayList<>();
        ArrayList<testrun> passAgain = new ArrayList<>();
        ArrayList<testrun> failAgain = new ArrayList<>();
        ArrayList<testrun> failNew = new ArrayList<>();
        ArrayList<testrun> passNew = new ArrayList<>();
        ArrayList<testrun> other = new ArrayList<>();



        for ( int indexMarket=0; indexMarket < testEnvironment.getMarkets().length; indexMarket++){
            for ( int indexFeature=0; indexFeature < testEnvironment.getMarkets()[indexMarket].getFeatures().length; indexFeature++){
                for (int indexTestRun=0; indexTestRun < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().size(); indexTestRun++){
                    //CHECK if the test run has only 1 execution, and add it based on that into NEW Passed or Failed list.
                    if (testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().size() == 1){
                        String onlyResult = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(0).getResult();
                        if (onlyResult.equalsIgnoreCase("passed")){
                            passNew.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        if (onlyResult.equalsIgnoreCase("failed")){
                            failNew.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        other.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                    }
                    //If the test run has more than 1 results, then we do the comparison between the last 2 test run results and update the related lists based on the comparison.
                    else {
                        String currentResult = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(0).getResult();
                        String earlierResult = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(1).getResult();
                        /* Checking if Passed Again */
                        if (currentResult.equalsIgnoreCase(earlierResult) && currentResult.equalsIgnoreCase("passed")) {
                            passAgain.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        /* Checking if Failed Again */
                        if (currentResult.equalsIgnoreCase(earlierResult) && currentResult.equalsIgnoreCase("failed")) {
                            failAgain.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        /* Checking if new FAILURE */
                        if (currentResult.equalsIgnoreCase("failed") && earlierResult.equalsIgnoreCase("passed")) {
                            failNew.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        /* Checking if new PASS */
                        if (currentResult.equalsIgnoreCase("passed") && earlierResult.equalsIgnoreCase("failed")) {
                            passNew.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                            continue;
                        }
                        /* Collecting Others */  //--> e.g. only 1 existing run execution, blocked, n/a, etc...
                        other.add(testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun));
                    }
                }

            }
        }
        summary.add(passAgain);
        summary.add(failAgain);
        summary.add(failNew);
        summary.add(passNew);
        summary.add(other);
        return summary;
    }

    /**
     *  This method sets the included test run amount for Features, Markets and the Environment.
     * @param testEnvironment is the test environment which you are working with.
     */
    private void setTestRunAmounts (environment testEnvironment)  {
        Integer amountRunEnvironment = 0;
        for (int indexMarketAmount = 0; indexMarketAmount < testEnvironment.getMarkets().length; indexMarketAmount++) {
            Integer amountRunMarket = 0;
            for (int indexFeatureAmount = 0; indexFeatureAmount < testEnvironment.getMarkets()[indexMarketAmount].getFeatures().length; indexFeatureAmount ++){
                testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].setTestRunAmount();
                amountRunMarket+=testEnvironment.getMarkets()[indexMarketAmount].getFeatures()[indexFeatureAmount].getTestRunAmount();
            }
            testEnvironment.getMarkets()[indexMarketAmount].setTestRunAmount(amountRunMarket);
            amountRunEnvironment+=testEnvironment.getMarkets()[indexMarketAmount].getTestRunAmount();
        }
        testEnvironment.setTestRunAmount(amountRunEnvironment);
    }

    /**
     * This method summarize the passed, failed and other results on Feature level.
     * @param testEnvironment is the test environment which you are working with.
     */
    private void setResultAmountFeature(environment testEnvironment) {
        for (int indexMarket = 0; indexMarket < testEnvironment.getMarkets().length; indexMarket++){
            for (int indexFeature = 0; indexFeature < testEnvironment.getMarkets()[indexMarket].getFeatures().length; indexFeature++){
                for (int indexTestRun = 0; indexTestRun < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().size(); indexTestRun++){
                    for (int indexResult = 0; indexResult < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().size(); indexResult++){
                        String resultTemporary = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexResult).getResult();
                        if (resultTemporary.equalsIgnoreCase("passed")){
                            testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getPassed()[indexResult]++;
                            continue;
                        }
                        if (resultTemporary.equalsIgnoreCase("failed")){
                            testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getFailed()[indexResult]++;
                            continue;
                        }

                        testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getOther()[indexResult]++;
                    }
                }
            }
        }

    }

    /**
     * This method summarize the passed, failed and other results on Market level.
     * @param testEnvironment is the test environment which you are working with.
     */
    private void setResultAmountMarket(environment testEnvironment){
        for (int indexMarket = 0; indexMarket < testEnvironment.getMarkets().length; indexMarket++){
            for (int indexFeature = 0; indexFeature < testEnvironment.getMarkets()[indexMarket].getFeatures().length; indexFeature++){
                for (int indexResultPassed = 0; indexResultPassed < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getPassed().length ; indexResultPassed++){
                    testEnvironment.getMarkets()[indexMarket].getPassed()[indexResultPassed]+=testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getPassed()[indexResultPassed];
                }
                for (int indexResultFailed = 0; indexResultFailed < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getFailed().length ; indexResultFailed++) {
                    testEnvironment.getMarkets()[indexMarket].getFailed()[indexResultFailed] += testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getFailed()[indexResultFailed];
                }
                for (int indexResultOther = 0; indexResultOther < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getOther().length ; indexResultOther++) {
                    testEnvironment.getMarkets()[indexMarket].getOther()[indexResultOther] += testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getOther()[indexResultOther];
                }
            }
        }
    }

    /**
     * This method summarize the passed, failed and other results on Environment level.
     * @param testEnvironment is the test environment which you are working with.
     */
    private void setResultAmountEnv(environment testEnvironment){
        for (int indexMarket = 0; indexMarket < testEnvironment.getMarkets().length; indexMarket++){
            for (int indexResultPassed = 0; indexResultPassed < testEnvironment.getMarkets()[indexMarket].getPassed().length ; indexResultPassed++){
                testEnvironment.getPassed()[indexResultPassed]+=testEnvironment.getMarkets()[indexMarket].getPassed()[indexResultPassed];
            }
            for (int indexResultFailed = 0; indexResultFailed < testEnvironment.getMarkets()[indexMarket].getFailed().length ; indexResultFailed++) {
                testEnvironment.getFailed()[indexResultFailed] += testEnvironment.getMarkets()[indexMarket].getFailed()[indexResultFailed];
            }
            for (int indexResultOther = 0; indexResultOther < testEnvironment.getMarkets()[indexMarket].getOther().length ; indexResultOther++) {
                testEnvironment.getOther()[indexResultOther] += testEnvironment.getMarkets()[indexMarket].getOther()[indexResultOther];
            }
        }
    }

    public String getCredentials (InputStream inputstream) throws IOException {
        Properties properties = new Properties();
        // load the stream.
        try {
            properties.load(inputstream);
        } finally {
            try { inputstream.close(); } catch (Exception ex) {}
        }

        // read variable from stream.
        if (properties.getProperty("token") == null) {
            throw new IllegalArgumentException("The specified properties data" +
                    "doesn't contain the expected properties 'token'.");
        }

        return properties.getProperty("token");
    }

    public String getFailureClassificationId (String failureClassificationString) {
        String classification;
        switch (failureClassificationString) {
            case "Application Defect":
                classification = failureClassification.applicationDefect.label;
                break;
            case "Script issue":
                classification = failureClassification.scriptIssue.label;
                break;
            case "Content issue":
                classification = failureClassification.contentIssue.label;
                break;
            case "Access issue":
                classification = failureClassification.accessIssue.label;
                break;
            case "Data issue":
                classification = failureClassification.dataIssue.label;
                break;
            case "Configuration issue":
                classification = failureClassification.configurationIssue.label;
                break;
            case "Environment issue":
                classification = failureClassification.environmentIssue.label;
                break;
            default:
                classification = failureClassification.empty.label; //Empty
        }
        return classification;
    }

    public String getFailureVerificationId (String failureVerificationString) {
        String verification;
        switch (failureVerificationString) {
            case "Not verified manually":
                verification = failureManualVerification.notVerified.label;
                break;
            case "Pass manually":
                verification = failureManualVerification.passManually.label;
                break;
            case "Fail manually":
                verification = failureManualVerification.failManually.label;
                break;
            default:
                verification = failureManualVerification.empty.label;
                break;
        }
        return verification;
    }

    public void updateChangeList (ArrayList<testrun> arrayToCheck, ArrayList<testrun> arrayChangeList){
        //DO THE COMPARISON BETWEEN Failures - arrayToCheck testruns
        for (int runCounter = 0; runCounter < arrayToCheck.size(); runCounter++) {
            for (int executionRunCounter = 1; executionRunCounter < arrayToCheck.get(runCounter).getExecutions().size(); executionRunCounter++) {
                execution latestExecution = arrayToCheck.get(runCounter).getExecutions().get(0);
                execution comparedExecution = arrayToCheck.get(runCounter).getExecutions().get(executionRunCounter);
                if (
                        latestExecution.getClassificationOfFailure().isEmpty() && !comparedExecution.getClassificationOfFailure().isEmpty() &&
                                latestExecution.getCauseOfFailure().equalsIgnoreCase(comparedExecution.getCauseOfFailure()) &&
                                latestExecution.getStepOfFailure().equalsIgnoreCase(comparedExecution.getStepOfFailure())
                ) {
                    arrayToCheck.get(runCounter).setSimilarExecutionFailureId(executionRunCounter);
                    arrayChangeList.add(arrayToCheck.get(runCounter));
                    break;
                }
            }
        }
    }

    public void sendTestLogUpdateRequestsBasedOnComparison(Long projectId, ArrayList<testrun> arrayChangeList) throws Exception {
        httpRequestModify poster = new httpRequestModify();
        httpRequestsGet getter = new httpRequestsGet();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

        for (int i = 0; i < arrayChangeList.size(); i++) {
            Integer similarExectutionFailureId = arrayChangeList.get(i).getSimilarFailureId();
            String startDate = dateFormat.format(arrayChangeList.get(i).getExecutions().get(0).getExecutionStartDate())+"+00:00";
            String endDate = dateFormat.format(arrayChangeList.get(i).getExecutions().get(0).getExecutionEndDate())+"+00:00";
            Long testRunId = arrayChangeList.get(i).getTestRunId();
            Long testLogIdLatest = getter.getLatestTestLogId(projectId, arrayChangeList.get(i).getTestRunId());
            String classificationId = getFailureClassificationId(arrayChangeList.get(i).getExecutions().get(similarExectutionFailureId).getClassificationOfFailure());
            String verificationId = getFailureVerificationId(arrayChangeList.get(i).getExecutions().get(similarExectutionFailureId).getVerificationOfFailure());
            String comment = "";

            if (arrayChangeList.get(i).getExecutions().get(similarExectutionFailureId).getCommentOfFailure().contains("automated")) {
                comment = arrayChangeList.get(i).getExecutions().get(similarExectutionFailureId).getCommentOfFailure();
            } else {
                comment = arrayChangeList.get(i).getExecutions().get(similarExectutionFailureId).getCommentOfFailure()+"\\n modified by Automated Script";
            }

            HttpResponse response = poster.updateLatestTestLog(
                    projectId,
                    testRunId,
                    testLogIdLatest,
                    startDate,
                    endDate,
                    classificationId,
                    comment,
                    verificationId);

            if (response.getStatusLine().getStatusCode() != 200) {
                System.out.println(arrayChangeList.get(i).getMarket() + " : "
                        + arrayChangeList.get(i).getTestCaseName() + "\n");
            }
        }
    }

    public Number[]  getClassificationAmount (environment testEnvironment, String classification, Integer arraySize) {
        Number[] classificationAmount = new Number[arraySize];
        Integer[] loopHelper = new Integer[arraySize];
        for (int indexArray = 0 ; indexArray < arraySize ; indexArray++){
            classificationAmount[indexArray]=0;
            loopHelper[indexArray]=0;
        }

        if (classification.equalsIgnoreCase("Application Defect") ||
                classification.equalsIgnoreCase("Script issue") ||
                classification.equalsIgnoreCase("Content issue") ||
                classification.equalsIgnoreCase("Access issue") ||
                classification.equalsIgnoreCase("Data Defect") ||
                classification.equalsIgnoreCase("Configuration Defect") ||
                classification.equalsIgnoreCase("Environment Defect") ) {
            for (int indexMarket = 0; indexMarket < testEnvironment.getMarkets().length; indexMarket++) {
                for (int indexFeature = 0; indexFeature < testEnvironment.getMarkets()[indexMarket].getFeatures().length; indexFeature++) {
                    for (int indexTestRun = 0; indexTestRun < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().size(); indexTestRun++) {
                        for (int indexExecution = 0; indexExecution < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().size(); indexExecution++) {
                            if (
                                    testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getResult().equalsIgnoreCase("failed") &&
                                    testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getClassificationOfFailure().equalsIgnoreCase(classification)
                            ) {
                                loopHelper[indexExecution] += 1;
                            }
                        }
                    }
                }
            }
        }

        if (classification.equalsIgnoreCase("Not Classified")){
            for (int indexMarket = 0; indexMarket < testEnvironment.getMarkets().length; indexMarket++) {
                for (int indexFeature = 0; indexFeature < testEnvironment.getMarkets()[indexMarket].getFeatures().length; indexFeature++) {
                    for (int indexTestRun = 0; indexTestRun < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().size(); indexTestRun++) {
                        for (int indexExecution = 0; indexExecution < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().size(); indexExecution++) {
                            if (
                                    testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getResult().equalsIgnoreCase("failed") &&
                                            !testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getClassificationOfFailure().equalsIgnoreCase("Application Defect") &&
                                            !testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getClassificationOfFailure().equalsIgnoreCase("Script issue")  &&
                                            !testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getClassificationOfFailure().equalsIgnoreCase("Content issue")  &&
                                            !testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getClassificationOfFailure().equalsIgnoreCase("Access issue")  &&
                                            !testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getClassificationOfFailure().equalsIgnoreCase("Data Defect")  &&
                                            !testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getClassificationOfFailure().equalsIgnoreCase("Configuration Defect") &&
                                            !testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun).getExecutions().get(indexExecution).getClassificationOfFailure().equalsIgnoreCase("Environment Defect")
                            ) {
                                loopHelper[indexExecution] += 1;
                            }
                        }
                    }
                }
            }
        }
        for (int indexArray = 0 ; indexArray < arraySize ; indexArray++){
            classificationAmount[indexArray]=loopHelper[indexArray];
        }
        return classificationAmount;
    }

}
