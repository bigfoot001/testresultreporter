package tools;


import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import pojo.environment;
import pojo.execution;
import pojo.testrun;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class httpRequestModify {
    String baseUrl = "https://avon.qtestnet.com/api/v3/projects/";
    dataBuilder objectCreator = new dataBuilder();


    public HttpResponse updateLatestTestLog (Long projectId, Long testRunId, Long testLogId, String startDate, String endDate, String classification, String comment, String verification) throws Exception {
        String url = baseUrl+projectId.toString()+"/test-runs/"+testRunId.toString()+"/test-logs/"+testLogId.toString();
        HttpPut request = new HttpPut(url);

        String body = setBodyTestLogUpdate(testLogId,startDate,endDate,classification,comment,verification);
        StringEntity payload = new StringEntity(body,ContentType.APPLICATION_JSON);
        String bearer = objectCreator.getCredentials(httpRequestModify.class.getResourceAsStream("/qTestCredentials.properties"));
        setDefaultHeaderPUT(request,bearer,String.valueOf(payload.getContentLength()));
        request.setEntity(payload);


        return getResponseEntityPUT(request);
    }

    private void setDefaultHeaderPUT(HttpPut originalRequest, String bearer, String contentLength){

        originalRequest.setHeader("authorization",bearer);
        originalRequest.setHeader("cache-control","no-cache");
        originalRequest.setHeader("content-type","application/json");
//        originalRequest.setHeader("content-length",contentLength);
        originalRequest.setHeader("host","avon.qtestnet.com");
        originalRequest.setHeader("user-agent","AS-automation_reporter");
        originalRequest.setHeader("accept","*/*");
        originalRequest.setHeader("accept-encoding","gzip, deflate, br");

    }

    private String setBodyTestLogUpdate (Long testLogId, String startDate, String endDate, String classification, String comment, String verification) {
        String body = "{\"id\": "+testLogId+",\n" +
                "\"exe_start_date\": \""+startDate+"\",\n" +
                "\"exe_end_date\": \""+endDate+"\",\n" +
                "\"properties\": [\n" +
                "    {\n" +
                "        \"field_id\": 9880650,\n" +
                "        \"field_value\": "+classification+"\n" +
                "    },\n" +
                "    {\n" +
                "        \"field_id\": 9880657,\n" +
                "        \"field_value\": "+verification+"\n" +
                "    },    \n" +
                "    {\n" +
                "        \"field_id\": 9751519,\n" +
                "        \"field_value\": \""+comment+"\"\n" +
                "    }\n" +
                "],\n" +
                "\"status\": {\n" +
                "    \"links\": [],\n" +
                "    \"id\": 602,\n" +
                "    \"name\": \"Failed\"\n" +
                "}\n" +
                "}";

        return body;
    }

    private HttpResponse getResponseEntityPUT(HttpPut givenRequest) throws Exception {
        if (givenRequest.getMethod().equalsIgnoreCase("PUT")) {
            CloseableHttpClient httpClientAll = HttpClientBuilder.create().build();
            HttpResponse response = httpClientAll.execute(givenRequest);

            return response;
        }
        throw new Exception("You need to give a PUT request (HttpPut) to this method. In any other cases it fails.");
    }

    public void updateAllFailuresInGivenMarkets(environment testEnvironment, Long projectId, ArrayList<String> markets, String classification, String verification, String comments) throws Exception {
        httpRequestsGet getter = new httpRequestsGet();
        for ( int indexGivenMarkets = 0; indexGivenMarkets < markets.size(); indexGivenMarkets++) {
            for (int indexMarket = 0; indexMarket < testEnvironment.getMarkets().length; indexMarket++) {
                if (testEnvironment.getMarkets()[indexMarket].getName().equalsIgnoreCase(markets.get(indexGivenMarkets))) {
                    for (int indexFeature = 0; indexFeature < testEnvironment.getMarkets()[indexMarket].getFeatures().length; indexFeature++) {
                        for (int indexTestRun = 0; indexTestRun < testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().size(); indexTestRun++) {
                            testrun selectedTestRun = testEnvironment.getMarkets()[indexMarket].getFeatures()[indexFeature].getTestruns().get(indexTestRun);
                            execution latestExecution = selectedTestRun.getExecutions().get(0);
                            if (latestExecution.getResult().equalsIgnoreCase("failed") && latestExecution.getClassificationOfFailure().isEmpty()) {
                                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
                                String startDate = dateFormat.format(latestExecution.getExecutionStartDate()) + "+00:00";
                                String endDate = dateFormat.format(latestExecution.getExecutionEndDate()) + "+00:00";
                                Long testRunId = selectedTestRun.getTestRunId();
                                Long testLogIdLatest = getter.getLatestTestLogId(projectId, selectedTestRun.getTestRunId());

                                updateLatestTestLog(
                                        projectId,
                                        testRunId,
                                        testLogIdLatest,
                                        startDate,
                                        endDate,
                                        classification,
                                        comments,
                                        verification);

                            }
                        }
                    }
                }
            }
        }
    }

}
