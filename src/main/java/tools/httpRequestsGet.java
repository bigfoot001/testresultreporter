package tools;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.qas.qtest.api.auth.PropertiesQTestCredentials;
import org.qas.qtest.api.auth.QTestCredentials;
import org.qas.qtest.api.services.execution.TestExecutionService;
import org.qas.qtest.api.services.execution.TestExecutionServiceClient;
import org.qas.qtest.api.services.execution.model.GetLastLogRequest;
import org.qas.qtest.api.services.execution.model.GetTestCycleRequest;
import org.qas.qtest.api.services.execution.model.TestCycle;
import org.qas.qtest.api.services.execution.model.TestLog;

import java.io.IOException;

public class httpRequestsGet {
    String baseUrl = "https://avon.qtestnet.com/api/v3/projects/";

    public Long getLatestTestLogId(Long projectId, Long testRunId) throws IOException {
        QTestCredentials credentials = new PropertiesQTestCredentials(httpRequestsGet.class.getResourceAsStream("/qTestCredentials.properties"));
        TestExecutionService testExecutionService = new TestExecutionServiceClient(credentials);
        testExecutionService.setEndpoint("avon.qtestnet.com");
        GetLastLogRequest getLastLogRequest = new GetLastLogRequest().withProjectId(projectId).withTestRunId(testRunId);
        TestLog testLog = testExecutionService.getLastLog(getLastLogRequest);

        return testLog.getId();
    }

    /**
     *  This method gives back an HttpEntity which contains the last results of a specific test run.
     * @param projectId qTest project ID
     * @param testRunId qTest Test Run ID
     * @param pageSize shows how many results you would like to see on 1 page, max: 999
     * @param bearer can be copied from qTest after you logged into, and checked "API&SDK" section in the "Download qTest Resources" page.
     * @return Outcome will be an HttpEntity for later usage.
     * @throws Exception
     */
    public HttpEntity getTestRunLastResults(String projectId, String testRunId, String pageSize, String bearer) throws Exception {
        return getResponseEntity(setDefaultGETRequest(bearer,baseUrl+projectId+"/test-runs/"+testRunId+"/test-logs?pageSize="+pageSize+"&page=1"));
    }

    /**
     *  This method gives back a Test Cycle with all the descendant test cycles. (only test cycles)
     * @param projectId is related to the project in qTest which you are interested in
     * @param testCycleId is one of the test cycles of PROD, QAM or QAF Automation root test cycles
     * @return a TestCycle object with all the included test cycles.
     * @throws IOException
     */
    public TestCycle getAllTestCyclesInTestCycle(Long projectId, Long testCycleId) throws IOException {
        /*  SETUP THE REQUEST  */
        String endpoint = "avon.qtestnet.com";
        String pathCredentials = "/qTestCredentials.properties";
        QTestCredentials credentials = new PropertiesQTestCredentials(dataBuilder.class.getResourceAsStream(pathCredentials));
        TestExecutionService testExecutionService = new TestExecutionServiceClient(credentials);
        testExecutionService.setEndpoint(endpoint);
        GetTestCycleRequest getTestCycleRequestDescendants = new GetTestCycleRequest().withProjectId(projectId).withTestCycleId(testCycleId);
        getTestCycleRequestDescendants.setIncludeDescendants(true);
        /*  GETTING THE TEST CYCLES */
        return testExecutionService.getTestCycle(getTestCycleRequestDescendants);
    }

    /**
     *  This method gives back all the existing test runs under the specified parent.
     * @param projectId qTest project ID
     * @param parentId ID of the "parentType" qTest item
     * @param pageSize shows how many results you would like to see on 1 page, max: 999
     * @param bearer can be copied from qTest after you logged into, and checked "API&SDK" section in the "Download qTest Resources" page.
     * @param parentType qTest item (starting point of search), can be only: root, release, test-cycle and test-suite
     * @return Outcome will be an HttpEntity for later usage.
     * @throws IOException
     */
    public HttpEntity getAllTestRunsForParent(String projectId, String parentId, String pageSize, String bearer, String parentType) throws Exception {
        return getResponseEntity(setDefaultGETRequest(bearer,baseUrl+projectId+"/test-runs?parentId="+ parentId+"&parentType="+parentType+"&expand=descendants&page=1&pageSize="+pageSize));
    }

    /**
     *  This method set up a basic GET request to qTest apis.
     * @param url endpoint where you would like to send your GET request
     * @param bearer can be copied from qTest after you logged into, and checked "API&SDK" section in the "Download qTest Resources" page.
     * @return Outcome will be a basic HttpGet entity with url and headers.
     */
    private HttpGet setDefaultGETRequest (String bearer, String url){
        HttpGet originalRequest = new HttpGet(url);
        originalRequest.setHeader("accept","application/json;charset=UTF-8");
        originalRequest.setHeader("accept-encoding","gzip");
        originalRequest.setHeader("authorization",bearer);
        originalRequest.setHeader("content-type","application/json;charset=UTF-8");
        originalRequest.setHeader("user-agent","http-auth-client-java/1.4.2 Windows_10/10.0 OpenJDK_64-Bit_Server_VM/11.0.4+10-b520.11");
        return originalRequest;
    }

    /**
     *  This method is providing and HttpEntity based on the response to your GET request.
     *  This entity contains the JSONObjects and JSONArrays for further usages.
     * @param givenRequest It should be an "HttpGet" entity already with url and headers.
     * @return Outcome will be an HttpEntity, the response for the GET request.
     * @throws Exception
     */
    private HttpEntity getResponseEntity (HttpGet givenRequest) throws Exception {
        if (givenRequest.getMethod().equalsIgnoreCase("GET")) {
            CloseableHttpClient httpClientAll = HttpClientBuilder.create().build();
            HttpResponse response = httpClientAll.execute(givenRequest);
            HttpEntity entity = response.getEntity();

            return entity;
        }
        throw new Exception("You need to give a GET request (HttpGet) to this method. In any other cases it fails.");
    }

}
