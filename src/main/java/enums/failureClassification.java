package enums;

public enum failureClassification {
    applicationDefect("1"),
    scriptIssue("2"),
    contentIssue("4"),
    accessIssue("5"),
    dataIssue("6"),
    configurationIssue("7"),
    environmentIssue("8"),
    empty("\"\"");

    public final String label;

    private failureClassification(String label) {
        this.label = label;
    }
}
