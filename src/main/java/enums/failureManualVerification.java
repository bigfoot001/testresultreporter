package enums;

public enum failureManualVerification {
    empty("\"\""),
    notVerified("1"),
    passManually("2"),
    failManually("3");

    public final String label;

    private failureManualVerification(String label) {
        this.label = label;
    }
}
