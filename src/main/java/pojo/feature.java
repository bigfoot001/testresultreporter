package pojo;

import java.util.ArrayList;

public class feature {
    public String name; //There is only Getter, to make sure that it won't change unintentionally.
    public Long testCycleId; //There is only Getter, to make sure that it won't change unintentionally.
    public Integer[] passed;
    public Integer[] failed;
    public Integer[] other;
    public ArrayList<testrun> testruns;
    public Integer testRunAmount;

    /**
     * This method construct an feature object with the name and the test cycle id.
     * @param name name of the feature
     * @param testCycleId id of the TestCycle which contains all the testruns for the given feature
     */
    public feature(String name, Long testCycleId) {
        this.name = name;
        this.testCycleId = testCycleId;
        //Instantiating the Result Arrays for last 10 execution
        this.passed = new Integer[]{0,0,0,0,0,0,0,0,0,0};
        this.failed = new Integer[]{0,0,0,0,0,0,0,0,0,0};
        this.other = new Integer[]{0,0,0,0,0,0,0,0,0,0};
        this.testruns = new ArrayList<testrun>();
        this.testRunAmount=0;
    }



    public String getName() {
        return name;
    }

    public Integer[] getPassed() {
        return passed;
    }

    public Integer[] getFailed() {
        return failed;
    }

    public Integer[] getOther() {
        return other;
    }

    public Long getTestCycleId() {
        return testCycleId;
    }

    public ArrayList<testrun> getTestruns() {
        return testruns;
    }

    public void setTestruns(ArrayList<testrun> testruns) {
        this.testruns = testruns;
    }

    public Integer getTestRunAmount() {
        return testRunAmount;
    }

    public void setTestRunAmount() {
        this.testRunAmount = this.testruns.size();
    }

}
