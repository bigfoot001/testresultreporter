package pojo;

public class market {
    String name; //There is only Getter, to make sure that it won't change unintentionally.
    Long testCycleId; //There is only Getter, to make sure that it won't change unintentionally.
    feature[] features;
    Integer testRunAmount;
    public Integer[] passed;
    public Integer[] failed;
    public Integer[] other;

    /**
     * This method construct a market object with the name and the test cycle id.
     * @param name name of the market
     * @param testCycleId id of the TestCycle which contains all the testruns for the given market
     */
    public market(String name, Long testCycleId) {
        this.name = name;
        this.testCycleId = testCycleId;
        this.testRunAmount=0;
        this.passed = new Integer[]{0,0,0,0,0,0,0,0,0,0};
        this.failed = new Integer[]{0,0,0,0,0,0,0,0,0,0};
        this.other = new Integer[]{0,0,0,0,0,0,0,0,0,0};
    }

    public String getName() {
        return name;
    }

    public Long getTestCycleId() {
        return testCycleId;
    }

    public feature[] getFeatures() {
        return features;
    }

    public void setFeatures(feature[] features) {
        this.features = features;
    }

    public Integer getTestRunAmount() {
        return testRunAmount;
    }

    public void setTestRunAmount(Integer testRunAmount) {
        this.testRunAmount = testRunAmount;
    }

    public Integer[] getPassed() {
        return passed;
    }

    public Integer[] getFailed() {
        return failed;
    }

    public Integer[] getOther() {
        return other;
    }
}
