package pojo;

public class environment {
    String name; //There is only Getter, to make sure that it won't change unintentionally.
    Long projectId; //There is only Getter, to make sure that it won't change unintentionally.
    Long testCycleId; //There is only Getter, to make sure that it won't change unintentionally.
    Integer[] passed;
    Integer[] failed;

    Integer[] other;

    market[] markets;
    Integer testRunAmount;

    /**
     * This method construct an environment object with the name and the test cycle id.
     * @param name name of the environment
     * @param testCycleId id of the TestCycle which contains all the testruns for the given environment
     */
    public environment(String name,Long testCycleId, Long projectId){
        this.name = name;
        this.testCycleId = testCycleId;
        this.projectId=projectId;
        this.testRunAmount=0;
        this.passed = new Integer[]{0,0,0,0,0,0,0,0,0,0};
        this.failed = new Integer[]{0,0,0,0,0,0,0,0,0,0};
        this.other = new Integer[]{0,0,0,0,0,0,0,0,0,0};
    }

    public String getName() {
        return name;
    }

    public market[] getMarkets() {
        return markets;
    }

    public void setMarkets(market[] markets) {
        this.markets = markets;
    }

    public Long getTestCycleId() {
        return testCycleId;
    }

    public Integer getTestRunAmount() {
        return testRunAmount;
    }

    public void setTestRunAmount(Integer testRunAmount) {
        this.testRunAmount = testRunAmount;
    }

    public Long getProjectId() {
        return projectId;
    }

    public Integer[] getPassed() {
        return passed;
    }

    public Integer[] getFailed() {
        return failed;
    }

    public Integer[] getOther() {
        return other;
    }
}
