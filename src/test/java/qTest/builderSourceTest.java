package qTest;


import enums.failureClassification;
import enums.failureManualVerification;
import org.apache.http.HttpResponse;
import org.apache.poi.ss.usermodel.Workbook;
import org.junit.Test;
import org.qas.qtest.api.auth.PropertiesQTestCredentials;
import org.qas.qtest.api.auth.QTestCredentials;
import org.qas.qtest.api.services.design.TestDesignService;
import org.qas.qtest.api.services.design.TestDesignServiceClient;
import org.qas.qtest.api.services.design.model.GetTestCaseRequest;
import org.qas.qtest.api.services.design.model.TestCase;
import pojo.environment;
import pojo.testrun;
import tools.buildExcelWorkbook;
import tools.dataBuilder;
import tools.httpRequestModify;
import tools.httpRequestsGet;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;

public class builderSourceTest {
    String endpoint = "avon.qtestnet.com";
    String pathCredentials = "/qTestCredentials.properties";
    Long projectId = Long.valueOf(92680);
    Long testCycleId = Long.valueOf(3663602);//QAM - 3663602 ; PROD - 3666111 ; QAF - 3704990;
    Long testRunId = Long.valueOf(141226330);
    Long testCaseId = Long.valueOf(39156855);
    Long testCaseVersion = Long.valueOf(57125305);

   @Test
   public void updateCurrentFailuresANDcreateWorkBook() throws Exception {
       dataBuilder objectCreator = new dataBuilder();
       httpRequestsGet requestCreator = new httpRequestsGet();
       String bearer = objectCreator.getCredentials(builderSourceTest.class.getResourceAsStream(pathCredentials));
       testrun[] testruns = objectCreator.createTestRuns(
               objectCreator.getTestRunsFromResponse(
                       requestCreator.getAllTestRunsForParent(projectId.toString(), testCycleId.toString(), "999", bearer, "test-cycle")
               ),
               projectId.toString(),
               bearer,
               10
       );

       environment testEnvironment = objectCreator.createTestEnvironment(projectId, testCycleId);
       objectCreator.buildUpTestEnvironment(testruns, testEnvironment, projectId);

       ArrayList<ArrayList<testrun>> testRunsGrouped = objectCreator.getLastTwoResultsForComparison(testEnvironment);
        /*
        0 - passAgain
        1 - failAgain
        2 - failNew
        3 - passNew
        4 - other
         */
       ArrayList<testrun> testRunsFailedAgain = testRunsGrouped.get(1);
       ArrayList<testrun> testRunsProposedForCopy = new ArrayList<>();
       ArrayList<testrun> testRunsNewFailed = testRunsGrouped.get(2);
       //DO THE COMPARISON BETWEEN Failures - FAILED AGAIN group (last 2 failed)
       objectCreator.updateChangeList(testRunsFailedAgain,testRunsProposedForCopy);
       //DO THE COMPARISON BETWEEN Failures - NEW FAILED group (last failed, previous not failed)
       objectCreator.updateChangeList(testRunsNewFailed,testRunsProposedForCopy);

       //Send the PUT requests to update the testruns
       objectCreator.sendTestLogUpdateRequestsBasedOnComparison(projectId,testRunsProposedForCopy);


       //CREATE WORKBOOK

       buildExcelWorkbook workbookCreator = new buildExcelWorkbook();
       Workbook workbookQualityReport = workbookCreator.createWorkbookQualityReport(testEnvironment);
       workbookCreator.createFile(workbookQualityReport);


   }
   @Test
   public void createStructureWithDetails() throws Exception {
       dataBuilder objectCreator = new dataBuilder();
       httpRequestsGet requestCreator = new httpRequestsGet();
       String bearer = objectCreator.getCredentials(builderSourceTest.class.getResourceAsStream(pathCredentials));
       testrun[] testruns =  objectCreator.createTestRuns(
               objectCreator.getTestRunsFromResponse(
                       requestCreator.getAllTestRunsForParent(projectId.toString(),testCycleId.toString(),"999",bearer,"test-cycle")
               ),
               projectId.toString(),
               bearer,
               10
       );

       environment testEnvironment = objectCreator.createTestEnvironment(projectId,testCycleId);
       objectCreator.buildUpTestEnvironment(testruns,testEnvironment,projectId);

       System.out.println("GYOZELEM!");

   }

    /**
     * Can fetch details of a test case.
     * @throws IOException ...
     */
    @Test
    public void read1TestCase() throws IOException {

        QTestCredentials credentials = new PropertiesQTestCredentials(builderSourceTest.class.getResourceAsStream(pathCredentials));

        TestDesignService testDesignService = new TestDesignServiceClient(credentials);
        testDesignService.setEndpoint(endpoint);

        GetTestCaseRequest getTestCaseRequest = new GetTestCaseRequest().withProjectId(projectId).withTestCaseId(testCaseId).withTestCaseVersion(testCaseVersion);
        TestCase testCase = testDesignService.getTestCase(getTestCaseRequest);

        System.out.println("break point");

    }

    /**
     * Update test log.
     * @throws IOException ...
     */
    @Test
    public void updateTestLog() throws Exception {
        httpRequestModify poster = new httpRequestModify();
        HttpResponse response = poster.updateLatestTestLog(projectId,(long)143177382,(long)207657595,
                "2020-09-10T11:04:20+00:00","2020-09-10T11:04:20+00:00",
                failureClassification.applicationDefect.label,
                "Typo in button \\nid - changed by script \\n8132",
                failureManualVerification.failManually.label);
    }

    /**
     * Highlight the differences on the last 2 runs
     * @throws Exception ...
     */
    @Test
    public void getDifferencesLastTwoRun() throws Exception {
        dataBuilder objectCreator = new dataBuilder();
        httpRequestsGet requestCreator = new httpRequestsGet();
        String bearer = objectCreator.getCredentials(builderSourceTest.class.getResourceAsStream(pathCredentials));
        testrun[] testruns =  objectCreator.createTestRuns(
                objectCreator.getTestRunsFromResponse(
                        requestCreator.getAllTestRunsForParent(projectId.toString(),testCycleId.toString(),"999",bearer,"test-cycle")
                ),
                projectId.toString(),
                bearer,
                2
        );

        ArrayList<testrun> better = new ArrayList<>();
        ArrayList<testrun> worst = new ArrayList<>();
        ArrayList<testrun> passedBoth = new ArrayList<>();
        ArrayList<testrun> failedBoth = new ArrayList<>();

        for (int runIndex = 0; runIndex < testruns.length; runIndex++) {
            if (testruns[runIndex].getExecutions().get(0).getResult().equalsIgnoreCase(testruns[runIndex].getExecutions().get(1).getResult()) && testruns[runIndex].getExecutions().get(0).getResult().equalsIgnoreCase("passed")){
                passedBoth.add(testruns[runIndex]);
            }
            if (testruns[runIndex].getExecutions().get(0).getResult().equalsIgnoreCase(testruns[runIndex].getExecutions().get(1).getResult()) && testruns[runIndex].getExecutions().get(0).getResult().equalsIgnoreCase("failed")){
                failedBoth.add(testruns[runIndex]);
            }
            if (testruns[runIndex].getExecutions().get(0).getResult().equalsIgnoreCase("passed") && testruns[runIndex].getExecutions().get(1).getResult().equalsIgnoreCase("failed")){
                worst.add(testruns[runIndex]);
            }
            if (testruns[runIndex].getExecutions().get(0).getResult().equalsIgnoreCase("failed") && testruns[runIndex].getExecutions().get(1).getResult().equalsIgnoreCase("passed")){
                better.add(testruns[runIndex]);
            }
        }
        System.out.println("WIN!");
    }

    /**
     * This test checks if there is any test run which failed with the same exception at the same step and if yes, then  copies the  classification, verification and comment into the current run's last execution
     * @throws Exception
     */
    @Test
    public void updateLastFailedTestLogBasedOnPreviousOnes() throws Exception {
        dataBuilder objectCreator = new dataBuilder();
        httpRequestsGet requestCreator = new httpRequestsGet();
        String bearer = objectCreator.getCredentials(builderSourceTest.class.getResourceAsStream(pathCredentials));
        testrun[] testruns = objectCreator.createTestRuns(
                objectCreator.getTestRunsFromResponse(
                        requestCreator.getAllTestRunsForParent(projectId.toString(), testCycleId.toString(), "999", bearer, "test-cycle")
                ),
                projectId.toString(),
                bearer,
                10
        );

        environment testEnvironment = objectCreator.createTestEnvironment(projectId, testCycleId);
        objectCreator.buildUpTestEnvironment(testruns, testEnvironment, projectId);

        ArrayList<ArrayList<testrun>> testRunsGrouped = objectCreator.getLastTwoResultsForComparison(testEnvironment);
        /*
        0 - passAgain
        1 - failAgain
        2 - failNew
        3 - passNew
        4 - other
         */
        ArrayList<testrun> testRunsFailedAgain = testRunsGrouped.get(1);
        ArrayList<testrun> testRunsProposedForCopy = new ArrayList<>();
        //DO THE COMPARISON BETWEEN Failures - FAILED AGAIN group (last 2 failed)
        objectCreator.updateChangeList(testRunsFailedAgain,testRunsProposedForCopy);

        ArrayList<testrun> testRunsNewFailed = testRunsGrouped.get(2);
        //DO THE COMPARISON BETWEEN Failures - NEW FAILED group (last failed, previous not failed)
        objectCreator.updateChangeList(testRunsNewFailed,testRunsProposedForCopy);

        //Send the PUT requests to update the testruns
        objectCreator.sendTestLogUpdateRequestsBasedOnComparison(projectId,testRunsProposedForCopy);

        System.out.println("Let's check it.");
    }

    @Test
    public void updateAllFailuresInGivenMarkets() throws Exception {
        dataBuilder objectCreator = new dataBuilder();
        httpRequestsGet requestCreator = new httpRequestsGet();
        String bearer = objectCreator.getCredentials(builderSourceTest.class.getResourceAsStream(pathCredentials));

        testrun[] testruns = objectCreator.createTestRuns(
                objectCreator.getTestRunsFromResponse(
                        requestCreator.getAllTestRunsForParent(projectId.toString(), testCycleId.toString(), "999", bearer, "test-cycle")
                ),
                projectId.toString(),
                bearer,
                10
        );

        environment testEnvironment = objectCreator.createTestEnvironment(projectId, testCycleId);
        objectCreator.buildUpTestEnvironment(testruns, testEnvironment, projectId);

        httpRequestModify poster = new httpRequestModify();
        ArrayList<String> markets = new ArrayList<String>(

                //2020.02.01
                //                Arrays.asList("CZ", "TR", "JT", "HU", "SK", "RO", "PL", "BG", "EE", "GE", "LV", "LT", "RS", "UK", "CL", "ZA", "DE")
                //2020.02.02
                Arrays.asList("ZA", "DE")
        );
        String comments = "Product categories are not available due to missing active campaign.\\n modified by Automated Script";
        String classification = failureClassification.contentIssue.label;
        String verification = failureManualVerification.failManually.label;

        poster.updateAllFailuresInGivenMarkets(
                testEnvironment,projectId,markets,classification,verification,comments
        );




    }


    /**
     * THIS is the "test" which can generate an excel report based on the currently available methods.
     * @throws Exception ...
     */
    @Test
    public void createWorkBookOverallReportAutomated() throws Exception {
        dataBuilder objectCreator = new dataBuilder();
        httpRequestsGet requestCreator = new httpRequestsGet();
        String bearer = objectCreator.getCredentials(builderSourceTest.class.getResourceAsStream(pathCredentials));
        testrun[] testruns =  objectCreator.createTestRuns(
                objectCreator.getTestRunsFromResponse(
                        requestCreator.getAllTestRunsForParent(projectId.toString(),testCycleId.toString(),"999",bearer,"test-cycle")
                ),
                projectId.toString(),
                bearer,
                10
        );
        environment testEnvironment = objectCreator.createTestEnvironment(projectId,testCycleId);
        objectCreator.buildUpTestEnvironment(testruns,testEnvironment,projectId);
        buildExcelWorkbook workbookCreator = new buildExcelWorkbook();
        Workbook workbookQualityReport = workbookCreator.createWorkbookQualityReport(testEnvironment);

        workbookCreator.createFile(workbookQualityReport);

    }




}

